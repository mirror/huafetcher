#Copyright (C) 2021 Andreas Shimokawa
#
#This file is part of Gadgetbridge-tools.
#
#Gadgetbridge is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published
#by the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#Gadgetbridge is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

with open("typemap.json") as f:
    jsonmap = json.load(f)

typemap={int(k,16):v for k,v in jsonmap.items()}

